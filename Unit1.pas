unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, NxColumns6, NxGridView6,
  NxControls6, NxCustomGrid6, NxVirtualGrid6, NxGrid6, Data.DB,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection,
  ZConnection, NxDBGrid6, Vcl.Imaging.pngimage, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    NextGrid61: TNextGrid6;
    NxReportGridView61: TNxReportGridView6;
    NxTextColumn6Cest: TNxTextColumn6;
    NxCheckBoxColumn61: TNxCheckBoxColumn6;
    Button1: TButton;
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    DataSource1: TDataSource;
    ZQuery1cest: TWideStringField;
    ZQuery1ncm: TWideStringField;
    ZQuery1descricao: TWideStringField;
    NxTextColumn6Descricao: TNxTextColumn6;
    NxTextColumn6Ncm: TNxTextColumn6;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
with ZQuery1 do
  begin
  First;
  NextGrid61.ClearRows;
  while not eof do
    begin
    NextGrid61.AddRow;
    NextGrid61.Cell[NxTextColumn6Cest.Index, NextGrid61.LastAddedRow].AsString := ZQuery1cest.AsString;
    NextGrid61.Cell[NxTextColumn6Ncm.Index, NextGrid61.LastAddedRow].AsString := ZQuery1ncm.AsString;
    NextGrid61.Cell[NxTextColumn6Descricao.Index, NextGrid61.LastAddedRow].AsString := ZQuery1descricao.AsString;
  //NextGrid61.Cell[NxCheckBoxColumn61.Index, NextGrid61.LastAddedRow].AsBoolean := true;
    next;
    end;
  end;
end;

end.
